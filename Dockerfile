FROM openjdk:8

WORKDIR /app

COPY target/standalone-0.0.1.RELEASE.jar /app/standalone.jar
COPY src/main/resources/application.yaml /app/config/application.yaml
COPY entrypoint.sh /app/entrypoint.sh

RUN mkdir -p /var/log/standalone && chmod +x /app/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
