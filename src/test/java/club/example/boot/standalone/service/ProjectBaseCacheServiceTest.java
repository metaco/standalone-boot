package club.example.boot.standalone.service;

import club.example.boot.standalone.domain.ProjectBase;
import club.example.boot.standalone.mapper.ProjectBaseMapper;
import club.example.boot.standalone.service.impl.ProjectBaseCacheServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProjectBaseCacheServiceTest {

    @Autowired
    private ProjectBaseCacheServiceImpl projectBaseCacheService;

    @Autowired
    private ProjectBaseMapper projectBaseMapper;

    @Test
    public void testSave() {
        ProjectBase projectBase = projectBaseMapper.findById(1);
        Assertions.assertNotNull(projectBase, "can not find projectBase by 38");

        boolean saved = projectBaseCacheService.save(projectBase, 120);
        Assertions.assertNotEquals(false, saved);
    }
}
