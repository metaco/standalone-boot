package club.example.boot.standalone.mapper;

import club.example.boot.standalone.domain.ProjectBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;

@SpringBootTest
public class ProjectBaseMapperTest {

    @Autowired
    private ProjectBaseMapper projectBaseMapper;

    @Test
    public void testInsert() {
        long affectedRow = projectBaseMapper.insert("KBaseProjectNameGene",
                "project-property", "projectDescription", new BigDecimal("20.1"));
        Assertions.assertNotEquals(0, affectedRow);
    }

    @Test
    public void testFind() {
        ProjectBase projectBase = projectBaseMapper.findById(1);
        Assertions.assertNotNull(projectBase, "project can not find ");
    }

    @Test
    public void testFindAll() {
        List<ProjectBase> projectBaseList = projectBaseMapper.findAll();
        Assertions.assertNotEquals(0, projectBaseList.size());
    }

    @Test
    public void testUpdate() {
        int updateAffectedRow = projectBaseMapper.updateById("project-property-update",
                "updateName", new BigDecimal("0.23"), 1L);
        Assertions.assertNotEquals(0, updateAffectedRow);
    }
}
