package club.example.boot.standalone.service;

import club.example.boot.standalone.domain.ProjectBase;
import club.example.boot.standalone.model.ProjectBaseModel;

import java.util.List;

public interface ProjectBaseService {

    ProjectBase get(long projectId);

    List<ProjectBase> getAll();

    ProjectBase create(ProjectBaseModel projectBase);

    ProjectBase update(ProjectBaseModel projectBase, long projectId);
}
