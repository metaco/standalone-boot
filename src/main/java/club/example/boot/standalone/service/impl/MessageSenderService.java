package club.example.boot.standalone.service.impl;

import club.example.boot.standalone.event.BlockedListEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageSenderService implements ApplicationEventPublisherAware {

    private final Logger logger = LoggerFactory.getLogger(MessageSenderService.class);

    private List<String> blockedList;

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void setBlockedList(List<String> blockedList) {
        this.blockedList = blockedList;
    }


    public void send(String address, String content) {
        if (blockedList.contains(address)) {
            logger.info("blocked address:{}", address);
            applicationEventPublisher.publishEvent(new BlockedListEvent(this, address, content));
            return ;
        }
        logger.info("send message..");
    }
}
