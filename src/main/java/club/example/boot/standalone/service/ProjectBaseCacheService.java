package club.example.boot.standalone.service;

import club.example.boot.standalone.domain.ProjectBase;

// ProjectBase 实体缓存操作服务
public interface ProjectBaseCacheService {

    boolean save(ProjectBase projectBase, long durationSeconds);

    ProjectBase get(long projectId);
}
