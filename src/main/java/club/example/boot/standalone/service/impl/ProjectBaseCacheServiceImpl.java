package club.example.boot.standalone.service.impl;

import club.example.boot.standalone.domain.ProjectBase;
import club.example.boot.standalone.exception.ProjectBaseException;
import club.example.boot.standalone.service.ProjectBaseCacheService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
public class ProjectBaseCacheServiceImpl implements ProjectBaseCacheService {

    private final static String CACHE_KEY_TEMPLATE = "project-base:id:%d";

    private final StringRedisTemplate stringRedisTemplate;

    private ValueOperations<String, String> stringValueOperation;

    private final ObjectMapper objectMapper;

    public ProjectBaseCacheServiceImpl(StringRedisTemplate stringRedisTemplate, ObjectMapper objectMapper) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void initValueOperation() {
        Assert.notNull(stringRedisTemplate, "stringRedisTemplate can not be null");
        stringValueOperation = stringRedisTemplate.opsForValue();
        Assert.notNull(stringValueOperation, "stringValueOperation can not be null");
        log.info("stringValueOperation init...");
    }

    @Override
    public boolean save(ProjectBase projectBase, long durationSeconds) {
        Objects.requireNonNull(projectBase, "projectBase can not be null");
        if (null == projectBase.getId() || 0 == projectBase.getId()) {
            throw new ProjectBaseException("projectBase id was null or 0");
        }

        try {
            String cacheValue = objectMapper.writeValueAsString(projectBase);
            String cacheKey = String.format(CACHE_KEY_TEMPLATE, projectBase.getId());
            stringValueOperation.set(cacheKey, cacheValue, durationSeconds, TimeUnit.SECONDS);
            return true;
        } catch (JsonProcessingException e) {
            log.error("write cache value occur error:{}", e.getMessage());
            throw new ProjectBaseException(e);
        }
    }

    @Override
    public ProjectBase get(long projectId) {
        String cacheKey = String.format(CACHE_KEY_TEMPLATE, projectId);
        Boolean exist = stringRedisTemplate.hasKey(cacheKey);
        if (null != exist && exist) {
            String cacheValue = stringValueOperation.get(cacheKey);
            try {
                return objectMapper.readValue(cacheValue, ProjectBase.class);
            } catch (JsonProcessingException e) {
                log.error("read cache value occur error:{}", e.getMessage());
                throw new ProjectBaseException(e);
            }
        }
        return null;
    }
}
