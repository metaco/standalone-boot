package club.example.boot.standalone.service.impl;

import club.example.boot.standalone.domain.ProjectBase;
import club.example.boot.standalone.mapper.ProjectBaseMapper;
import club.example.boot.standalone.model.ProjectBaseModel;
import club.example.boot.standalone.service.ProjectBaseService;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Log4j2
@Service
public class ProjectBaseServiceImpl implements ProjectBaseService {

    private final ProjectBaseMapper projectBaseMapper;

    private final MessageSenderService messageSenderService;

    public ProjectBaseServiceImpl(ProjectBaseMapper projectBaseMapper, MessageSenderService messageSenderService) {
        this.projectBaseMapper = projectBaseMapper;
        this.messageSenderService = messageSenderService;
    }

    @PostConstruct
    public void initSenderBlockedList() {
        List<String> blockedAddressList = Arrays.asList("linframe@outlook.com", "wangful15@outlook.com");
        messageSenderService.setBlockedList(blockedAddressList);
        log.info("init blocked list...");
    }

    @Override
    public ProjectBase get(long projectId) {
        log.info("get projectId:{}", projectId);
        ProjectBase projectBase = projectBaseMapper.findById(projectId);
        if (null == projectBase) {
            log.info("no result for projectId:{}", projectId);
            return null;
        }
        log.info("find projectId:{}", projectId);
        messageSenderService.send("wangful15@outlook.com", "content...");
        return projectBase;
    }

    @Override
    public List<ProjectBase> getAll() {
        log.info("getAll(no args)");
        List<ProjectBase> projectBases = projectBaseMapper.findAll();
        if (CollectionUtils.isEmpty(projectBases)) {
            return Collections.emptyList();
        }
        return projectBases;
    }

    @Override
    public ProjectBase create(ProjectBaseModel projectBase) {
        log.info("create projectBase:{}", projectBase.toString());
        ProjectBase base = new ProjectBase();
        BeanUtils.copyProperties(projectBase, base);
        long affectedRow = projectBaseMapper.insert(base.getName(), base.getProperty(),
                base.getDescription(), base.getBaseQuota());
        return affectedRow > 0 ? base : null;
    }

    @Override
    public ProjectBase update(ProjectBaseModel projectBase, long projectId) {
        log.info("update projectBase:{},projectId;{}", projectBase.toString(), projectId);
        int updatedRow = projectBaseMapper.updateById(projectBase.getProperty(), projectBase.getName(),
                projectBase.getBaseQuota(), projectId);
        ProjectBase projectBase1 = new ProjectBase();
        BeanUtils.copyProperties(projectBase, projectBase1);
        return updatedRow > 0 ? projectBase1 : null;
    }
}
