package club.example.boot.standalone.service.impl;

import club.example.boot.standalone.domain.AsyncResult;
import club.example.boot.standalone.domain.GithubUser;
import club.example.boot.standalone.service.ApplicationAsyncTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class GithubAsyncTaskService implements ApplicationAsyncTaskService {

    private static final Logger logger = LoggerFactory.getLogger(GithubAsyncTaskService.class);

    private final RestTemplate restTemplate;

    public GithubAsyncTaskService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Async
    @Override
    public CompletableFuture<AsyncResult> getSingleResult(String name) throws InterruptedException{
        logger.info("Looking up " + name);
        String url = String.format("https://api.github.com/users/%s", name);
        GithubUser results = restTemplate.getForObject(url, GithubUser.class);
        logger.info("response:{}", results);
        // Artificial delay of 1s for demonstration purposes
        // Thread.sleep(1000L);
        return CompletableFuture.completedFuture(results);
    }
}
