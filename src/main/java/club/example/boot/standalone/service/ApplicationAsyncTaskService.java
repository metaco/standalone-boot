package club.example.boot.standalone.service;

import club.example.boot.standalone.domain.AsyncResult;

import java.util.concurrent.CompletableFuture;

public interface ApplicationAsyncTaskService {

    CompletableFuture<AsyncResult> getSingleResult(String name) throws InterruptedException;
}
