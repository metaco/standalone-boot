package club.example.boot.standalone.mapper;

import club.example.boot.standalone.domain.ProjectBase;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ProjectBaseMapper {

    ProjectBase findById(@Param("id") long id);

    List<ProjectBase> findAll();

    long insert(@Param("name") String projectName,
               @Param("property") String property,
               @Param("description") String description,
               @Param("baseQuota") BigDecimal quota);

    int updateById(
            @Param("property") String property, @Param("name") String projectName,
    @Param("baseQuota") BigDecimal baseQuota, @Param("id") Long id);
}
