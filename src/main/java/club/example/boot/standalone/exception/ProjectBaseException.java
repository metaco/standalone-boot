package club.example.boot.standalone.exception;

public class ProjectBaseException extends RuntimeException {

    public ProjectBaseException(String message) {
        super(message);
    }

    public ProjectBaseException(Throwable cause) {
        super(cause);
    }
}
