package club.example.boot.standalone.configure;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Mybatis Mapper Scan Directory
 */
@Configuration
@MapperScan(basePackages = {"club.example.boot.standalone.mapper"})
public class MybatisMapperConfigure {
}
