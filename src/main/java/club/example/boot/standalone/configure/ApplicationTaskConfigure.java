package club.example.boot.standalone.configure;

import club.example.boot.standalone.configure.properties.AppTaskProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableConfigurationProperties(AppTaskProperties.class)
public class ApplicationTaskConfigure {

    private final AppTaskProperties appTaskProperties;

    public ApplicationTaskConfigure(AppTaskProperties appTaskProperties) {
        this.appTaskProperties = appTaskProperties;
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(appTaskProperties.getCoreSize());
        executor.setMaxPoolSize(appTaskProperties.getMaxSize());
        executor.setQueueCapacity(appTaskProperties.getQueueCapacity());
        executor.setThreadNamePrefix(appTaskProperties.getNamePrefix());
        executor.initialize();
        return executor;
    }
}
