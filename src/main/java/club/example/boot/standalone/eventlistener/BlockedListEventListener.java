package club.example.boot.standalone.eventlistener;

import club.example.boot.standalone.event.BlockedListEvent;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class BlockedListEventListener implements ApplicationListener<BlockedListEvent> {

    @Override
    public void onApplicationEvent(BlockedListEvent event) {
        log.info("blockedList event received...");
        log.info("blocked address:{}, content:{}", event.getAddress(), event.getContent());
    }
}
