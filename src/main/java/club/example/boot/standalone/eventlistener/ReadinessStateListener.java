package club.example.boot.standalone.eventlistener;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class ReadinessStateListener implements ApplicationListener<AvailabilityChangeEvent<ReadinessState>> {

    @Override
    public void onApplicationEvent(AvailabilityChangeEvent<ReadinessState> event) {
        ReadinessState readinessState = event.getState();
        if (ReadinessState.ACCEPTING_TRAFFIC == readinessState) {
            log.info("accepting traffic...");
        }

        if (ReadinessState.REFUSING_TRAFFIC == readinessState) {
            log.info("refusing traffic...");
        }
    }
}
