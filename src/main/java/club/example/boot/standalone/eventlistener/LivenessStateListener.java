package club.example.boot.standalone.eventlistener;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * spring-boot-features.html#boot-features-application-availability
 * @link https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-application-availability
 */
@Log4j2
@Component
public class LivenessStateListener implements ApplicationListener<AvailabilityChangeEvent<LivenessState>> {

    @Override
    public void onApplicationEvent(AvailabilityChangeEvent<LivenessState> event) {
        LivenessState livenessState = event.getState();
        switch (livenessState) {
            case BROKEN:
                log.info("application broken down...");
                break;
            case CORRECT:
                log.info("application correct.");
                break;
        }
    }
}
