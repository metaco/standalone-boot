package club.example.boot.standalone.eventlistener.application;

import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class ApplicationStartedEventListener implements ApplicationListener<ApplicationStartedEvent> {

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        log.info("onApplicationStartedEvent :{}", event.getSource());
        for (String eventArg : event.getArgs()) {
            log.info("onApplicationStartedEvent args:{}", eventArg);
        }
    }
}
