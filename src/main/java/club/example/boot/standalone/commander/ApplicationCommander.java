package club.example.boot.standalone.commander;

import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Component
public class ApplicationCommander implements CommandLineRunner {

    // java -jar app.jar --server.port=8002 --arguments=--person.name=test
    @Override
    public void run(String... args) {
        List<String> argList = Stream.of(args).collect(Collectors.toList());
        log.info("run CommandLine:{}", argList.toString());
    }
}
