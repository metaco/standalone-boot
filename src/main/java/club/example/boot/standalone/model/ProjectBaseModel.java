package club.example.boot.standalone.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class ProjectBaseModel {

    private String name;

    private String property;

    private String description;

    private BigDecimal baseQuota;
}
