package club.example.boot.standalone.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Setter
@Getter
@JsonIgnoreProperties(value = {"isDeleted"})
public class ProjectBase {

    /**
     * `id` bigint(20) NOT NULL AUTO_INCREMENT,
     *        `name` varchar(20) NOT NULL COMMENT '实体名字',
     *        `property` varchar(35) NOT NULL COMMENT '实体属性值',
     *        `description` varchar(35) NOT NULL DEFAULT '' COMMENT '项目描述',
     *        `status` enum('enable','disable') NOT NULL DEFAULT 'enable' COMMENT '实体状态',
     *        `base_quota` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '基本配额',
     *        `created_at` datetime NOT NULL COMMENT '创建时间',
     *        `updated_at` datetime NOT NULL COMMENT '最后一次修改时间',
     *        `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
     *                                PRIMARY KEY (`id`)
     */
    private Long id;

    private String name;

    private String property;

    private String description;

    private String status;

    private BigDecimal baseQuota;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;

    private Byte isDeleted;
}
