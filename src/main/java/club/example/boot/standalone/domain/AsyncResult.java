package club.example.boot.standalone.domain;

public interface AsyncResult {

    String fetchResultName();
}
