package club.example.boot.standalone.controller;

import club.example.boot.standalone.domain.AsyncResult;
import club.example.boot.standalone.domain.JsonResponse;
import club.example.boot.standalone.domain.ProjectBase;
import club.example.boot.standalone.model.ProjectBaseModel;
import club.example.boot.standalone.service.ApplicationAsyncTaskService;
import club.example.boot.standalone.service.ProjectBaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@Slf4j
@RestController
@RequestMapping("/project/base")
public class ProjectBaseController {

    private final ProjectBaseService projectBaseService;

    private final ApplicationAsyncTaskService applicationAsyncTaskService;

    public ProjectBaseController(ProjectBaseService projectBaseService,
                                 ApplicationAsyncTaskService applicationAsyncTaskService) {
        this.projectBaseService = projectBaseService;
        this.applicationAsyncTaskService = applicationAsyncTaskService;
    }

    @GetMapping("/{projectId}")
    public JsonResponse<ProjectBase> get(@PathVariable("projectId") Long projectId) {
        ProjectBase projectBase = projectBaseService.get(projectId);
        return new JsonResponse<>(0, "success", projectBase);
    }

    @PostMapping
    public JsonResponse<ProjectBase> post(@RequestBody ProjectBaseModel projectBaseModel) {
        ProjectBase projectBase = projectBaseService.create(projectBaseModel);
        return new JsonResponse<>(0, "success", projectBase);
    }

    @PutMapping("/{projectId}")
    public JsonResponse<ProjectBase> put(@PathVariable("projectId") Long projectId,
                                         @RequestBody ProjectBaseModel projectBaseModel) {
        ProjectBase projectBase = projectBaseService.update(projectBaseModel, projectId);
        return new JsonResponse<>(0, "success", projectBase);
    }

    @GetMapping("/github")
    public JsonResponse<String> getGithubUser(@RequestParam("userName") String userName) {
        log.info("getGithubUser username:{}", userName);
        try {
            CompletableFuture<AsyncResult> completableFuture = applicationAsyncTaskService
                    .getSingleResult(userName);
            CompletableFuture<AsyncResult> singleResult = applicationAsyncTaskService
                    .getSingleResult("CloudFoundry");
            return new JsonResponse<>(0, "success", "done");
        } catch (InterruptedException e) {
            return new JsonResponse<>(-1000, "failed");
        }
    }
}
