CREATE DATABASE IF NOT EXISTS `standalone` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

USE `standalone`;

CREATE TABLE IF NOT EXISTS `project_base` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT,
    `name` varchar(20) NOT NULL COMMENT '实体名字',
    `property` varchar(35) NOT NULL COMMENT '实体属性值',
    `description` varchar(35) NOT NULL DEFAULT '' COMMENT '项目描述',
    `status` enum('enable','disable') NOT NULL DEFAULT 'enable' COMMENT '实体状态',
    `base_quota` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '基本配额',
    `created_at` datetime NOT NULL COMMENT '创建时间',
    `updated_at` datetime NOT NULL COMMENT '最后一次修改时间',
    `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Standalone Project 属性测试'