#!/usr/bin/env bash

echo $(date) "server ready to starting.."

java -jar /app/standalone.jar --spring.config.location=/app/config/application.yaml

echo $(date) "server started."